
# mpvue-xbyjShop
> 基于mpvue的微信小程序商城（小程序端，服务端，后台管理）


# 小程序端

# 技术栈
> mpvue + mpvue-router-patch + mpvue-entry + vuex + webpack + ES6/7 + flyio + mpvue-wxparse

# 项目运行
```
微信开发中工具选中mpvue-xbyjShop/buyer作为项目目录即可
```

# 功能列表
## 页面
- [x] 首页 -- 完成
- [x] 分类商品 -- 完成
- [x] 商家品牌、品牌详情 -- 完成
- [x] 新品首发 -- 完成
- [x] 人气推荐 -- 完成
- [x] 专题商品、专题详情 -- 完成
- [x] 分类首页 -- 完成
- [x] 搜索页 -- 完成
- [x] 商品详情 -- 完成
- [x] 评论页 -- 完成
- [x] 购物车 -- 完成
- [x] 下单页 -- 完成
- [x] 支付页、支付结果页 -- 完成
- [x] 我的订单、订单详情页 -- 完成
- [ ] 优惠卷
- [x] 我的收藏 -- 完成
- [x] 我的足迹 -- 完成
- [x] 地址管理页 -- 完成
- [ ] 意见反馈
- [ ] 物流查询

## 组件
- [x] 商品筛选组件 -- 综合、价格、分类

## 功能
- [x] 专题评论
- [x] 搜索商品
- [x] 商品收藏
- [x] 加入购物车
- [x] 购物车商品的编辑、删除、批量操作
- [x] 浏览记录
- [x] 收货地址的增、删、改
- [x] 下单支付
.....

# 小程序效果展示

### 首页、商品分类页

<img src="http://open.mutou888.com/2.%E5%95%86%E5%93%81%E5%88%86%E7%B1%BB.png" width="320" height="619"/> <img src="http://open.mutou888.com/7.%E5%88%86%E7%B1%BB%E9%A6%96%E9%A1%B5.png" width="320" height="619"/>

### 品牌详情页、人气推荐页

<img src="http://open.mutou888.com/3.%E5%93%81%E7%89%8C%E8%AF%A6%E6%83%85%E9%A1%B5.png" width="320" height="619"/> <img src="http://open.mutou888.com/4.%E4%BA%BA%E6%B0%94%E6%8E%A8%E8%8D%90.gif" width="320" height="619"/>

### 专题、专题详情

<img src="http://open.mutou888.com/6.%E4%B8%93%E9%A2%98%E8%AF%A6%E6%83%85.gif" width="320" height="619"/> <img src="http://open.mutou888.com/5.%E4%B8%93%E9%A2%98.gif" width="320" height="619"/>

### 分类首页、搜索页

<img src="http://open.mutou888.com/2.%E5%95%86%E5%93%81%E5%88%86%E7%B1%BB.png" width="320" height="619"/> <img src="http://open.mutou888.com/8.%E6%90%9C%E7%B4%A2.gif" width="320" height="619"/>

### 商品详情、购物车

<img src="http://open.mutou888.com/10.%E8%B4%AD%E7%89%A9%E8%BD%A6.gif" width="320" height="619"/> <img src="http://open.mutou888.com/9.%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85.gif" width="320" height="619"/>

### 确认订单、付款页

<img src="http://open.mutou888.com/11.%E7%A1%AE%E8%AE%A4%E8%AE%A2%E5%8D%95.png" width="320" height="619"/> <img src="http://open.mutou888.com/14.%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83.png" width="320" height="619"/>

### 我的订单、订单详情

<img src="http://open.mutou888.com/15.%E6%88%91%E7%9A%84%E8%AE%A2%E5%8D%95.png" width="320" height="619"/> <img src="http://open.mutou888.com/16.%E8%AE%A2%E5%8D%95%E8%AF%A6%E6%83%85.png" width="320" height="619"/>

### 优惠卷、我的收藏

<img src="http://open.mutou888.com/17.%E4%BC%98%E6%83%A0%E5%8D%B7.png" width="320" height="619"/> <img src="http://open.mutou888.com/18.%E6%88%91%E7%9A%84%E6%94%B6%E8%97%8F.png" width="320" height="619"/>

### 我的足迹、地址管理

<img src="http://open.mutou888.com/19.%E6%88%91%E7%9A%84%E8%B6%B3%E8%BF%B9.png" width="320" height="619"/> <img src="http://open.mutou888.com/20.%E5%9C%B0%E5%9D%80%E7%AE%A1%E7%90%86.gif" width="320" height="619"/>

# 后台管理端展示，可定制开发

### 横幅管理

<img src="http://open.mutou888.com/admin%E6%9D%A1%E5%B9%85%E7%AE%A1%E7%90%86.jpg"/>

### 分类管理

<img src="http://open.mutou888.com/admin%E5%88%86%E7%B1%BB%E7%AE%A1%E7%90%86.jpg"/>

### 品牌管理

<img src="http://open.mutou888.com/admin%E5%93%81%E7%89%8C%E7%AE%A1%E7%90%86.jpg"/>

### 商品管理

<img src="http://open.mutou888.com/admin%E5%95%86%E5%93%81%E7%AE%A1%E7%90%86.jpg"/>

### 频道管理

<img src="http://open.mutou888.com/admin%E9%A2%91%E9%81%93%E7%AE%A1%E7%90%86.jpg"/>


# 服务端API

> 服务端api基于Ｎode.js+ThinkJS+MySQL

## 项目运行
```
创建数据库xbyjshop

导入mpvue-xbyjShop/server目录下的xbyjShop.sql数据

修改两个配置文件，见下面

安装依赖 npm install

启动项目 npm start

```

## 修改数据库配置文件 
server/src/common/config/database.js
```
const mysql = require('think-model-mysql');

module.exports = {
    handle: mysql,
    database: 'xbyjshop',
    prefix: 'xbyjshop_',
    encoding: 'utf8mb4',
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: '你的密码',
    dateStrings: true
};
```

## 修改微信登录和微信支付配置文件 
server/src/common/config/config.js
```
// default config
module.exports = {
  default_module: 'api',
  weixin: {
    appid: '', // 小程序 appid
    secret: '', // 小程序密钥
    mch_id: '', // 商户帐号ID
    partner_key: '', // 微信支付密钥
    notify_url: '' // 微信异步通知
  }
};
```



# 最后

1、小程序商城后台管理端

基于java Spring开发。

# 联系我

最新源码地址 [包含前台和后台](http://mutou888.com/pay/resource500.html "商城源码")
